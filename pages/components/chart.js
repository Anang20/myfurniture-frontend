import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    BarController,
    BarElement,
    ArcElement,
    DoughnutController,
    Title,
    Tooltip,
    Legend,
    Filler,
} from "chart.js";

ChartJS.register(
  BarController,
  BarElement,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  DoughnutController,
  ArcElement,
  Filler,
  Title,
  Tooltip,
  Legend,
);
import {Line, Bar, Doughnut} from 'react-chartjs-2';

const Chart = (props) => {

    const {customer,month,transaksi,pendapatan} = props

    const optionss = {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Statistik Transaksi Perbulan',
        },
      },
    };
  
    const data = {
      labels: month,
      datasets: [
        {
          label: 'Total Transaksi',
          fill: true,
          lineTension: 0.1,
          backgroundColor: 'rgba(75,192,192,0.4)',
          borderColor: 'rgba(75,192,192,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: transaksi,
        },
      ],
    };

    const data2 = {
      labels: month,
      datasets: [{
        label: 'Total Pendapatan',
        data: pendapatan,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }]
    }

    const optionsss = {
      elements: {
        arc: {
          weight: 0.5,
          borderWidth: 3,
        },
      },
      cutout: 150,
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Statistik Total Customer Perbulan',
        },
      },
    };

    const data3 = {
      backgroundColor: [
        "rgb(2,88,255)",
        "rgb(249,151,0)",
        "rgb(255,199,0)",
        "rgb(32,214,152)",
      ],
      labels: month,
      datasets: [
        {
          data: customer,
          backgroundColor: [
            "rgb(2,88,255)",
            "rgb(249,151,0)",
            "rgb(255,199,0)",
            "rgb(32,214,152)",
          ],
          hoverOffset: 4,
        },
      ],
    };

    const options = {
      elements: {
        arc: {
          weight: 0.5,
          borderWidth: 3,
        },
      },
      cutout: 150,
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Statistik Pendapatan Perbulan',
        },
      },
    };
    
    return (
        <div className='row'>
          <div className='col-6'>
            <div className='card shadow'>
                <Line
                data={data}
                width={10}
                height={10}
                options={optionss}
                />
            </div>
          </div>
          <div className='col-6' style={{ position: 'relative' }}>
            <div className='card shadow'>
                <Doughnut
                data={data3}
                width={30}
                height={30}
                options={optionsss}
                />
            </div>
          </div>
          <div className='col-12 mt-5'>
            <div className='card shadow'>
              <Bar
              data={data2}
              width={10}
              height={10}
              options={options}
              />
            </div>
          </div>
        </div>
    )
}
export default Chart