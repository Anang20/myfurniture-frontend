import { faChair, faChartArea, faDollar, faHandHoldingUsd, faMoneyBill, faUsers } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import Head from "next/head";
import { useEffect, useState } from "react";
import appConfig from "../../config/app";
import useAuthenticatedPage from "../../helper/useAuthenticatedPage";
import Chart from "../components/chart";
import NavbarAdmin from "../components/navbar_admin";
import SidebarAdmin from "../components/sidebar_admin";

const Dashboard = () => {
    
    useAuthenticatedPage();
    const [dataDashboard, setDataDashboard] = useState([])
    const [transaksi, setTransaksi] = useState([])
    const [customer, setCustomer] = useState([])
    const [pendapatan, setPendapatan] = useState([])
    const [totalPendapatan, setTotalPendapatan] = useState(0)
    const [month, setMonth] = useState([])

    useEffect(() => {
      const getTransaksi = async () => {
        const response = await axios.get(`${appConfig.apiUrl}/dashboard/data/transaksi`);
        const result = response.data
        const arr = [result]
        const tempArr = []
        arr.map(value => {
          tempArr.push(value.Now)
        });
        const values = Object.values(tempArr[0])
        setTransaksi(values);
      }
      getTransaksi()
    }, [])

    useEffect(() => {
      const getAllCustomer = async () => {
        const response = await axios.get(`${appConfig.apiUrl}/dashboard/data/registrasi`);
        const result = response.data
        const arr = [result]
        const array = []
        arr.map(value => {
          array.push(value.Now)
        })
        const keys = Object.keys(array[0])
        const values = Object.values(array[0])
        setCustomer(values)
        setMonth(keys)
      }
      getAllCustomer()
    }, [])

    useEffect(() => {
      const getAllPendapatan = async () => {
        const response = await axios.get(`${appConfig.apiUrl}/dashboard/data/pendapatan`);
        const pendapatanTotal = response.data.TotalOrder;
        setTotalPendapatan(pendapatanTotal);
        const result = response.data.Grafik;
        const arr = [result]
        const array = []
        arr.map(value => {
          array.push(value.Now)
        })
        console.log(array[0]);
        const keys = Object.keys(array[0])
        const values = Object.values(array[0])
        setPendapatan(values)
        setMonth(keys)
      }
      getAllPendapatan()
    }, [])

    const getData = async () => {
        const res = await axios.get(`${appConfig.apiUrl}/dashboard`);
        const result = res.data.data;
        setDataDashboard(result)
    }

    useEffect(() => {
        getData()
    }, [])

    const curency = (value)=>{
        const formatter = new Intl.NumberFormat('en-ID', {
            style: 'currency',
            currency: 'IDR'
            }).format(value)
            .replace(/[IDR]/gi, '')
            .replace(/(\.+\d{2})/, '')
            .trimLeft()
        return formatter
    }

    useAuthenticatedPage()

    return (
        <>
        <Head>
            <title>MyFuniture | Dashboard</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
         <div id="wrapper">
            <SidebarAdmin/>
                <div id="content-wrapper" className="d-flex flex-column" style={{ backgroundColor: '#FFFF' }}>
                    <div id="content">
                    <NavbarAdmin/>
                    <div className="container-fluid col-12" style={{ paddingLeft: 250, marginTop: 90}}>
                        <h4 className="text-gray-600">Dashboard</h4>
                        <div className="row">
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card shadow h-100 py-2" style={{ borderRadius: 30 }}>
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center mt-2">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-gray-800 text-uppercase mb-1 pl-3">
                                                    Total Produk
                                                </div>
                                                <div className="mb-0 font-weight-bold text-gray-800 pl-3"><p style={{ fontSize: 18 }}>{dataDashboard.produk} Produk</p></div>
                                            </div>
                                            <div className="col-auto pr-3 pb-3">
                                               <FontAwesomeIcon
                                               icon={faChair}
                                               color={'#A68787'}
                                               style={{ width: 45, height: 45 }}
                                               />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card shadow h-100 py-2" style={{ borderRadius: 30 }}>
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center mt-2">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-gray-800 text-uppercase mb-1 pl-3">
                                                    Total Customer
                                                </div>
                                                <div className="mb-0 font-weight-bold text-gray-800 pl-3"><p style={{ fontSize: 18 }}>{dataDashboard.user} Customer</p></div>
                                            </div>
                                            <div className="col-auto pr-3 pb-2">
                                               <FontAwesomeIcon
                                               icon={faUsers}
                                               color={'#926EF9'}
                                               style={{ width: 45, height: 45 }}
                                               />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card shadow h-100 py-2" style={{ borderRadius: 30 }}>
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center mt-2">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-gray-800 text-uppercase mb-1 pl-3">
                                                    Total Transaksi
                                                </div>
                                                <div className="mb-0 font-weight-bold text-gray-800 pl-3"><p style={{ fontSize: 18 }}>{dataDashboard.payment} Transaksi</p></div>
                                            </div>
                                            <div className="col-auto pr-3 pb-3">
                                               <FontAwesomeIcon
                                               icon={faChartArea}
                                               color={'orange'}
                                               style={{ width: 45, height: 45 }}
                                               />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card shadow h-100 py-2" style={{ borderRadius: 30 }}>
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center mt-2">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-gray-800 text-uppercase mb-1 pl-2">
                                                    Total Pendapatan
                                                </div>
                                                <div className="mb-0 font-weight-bold text-gray-800 pl-2"><p style={{ fontSize: 16 }}>Rp. {curency(totalPendapatan)}</p></div>
                                            </div>
                                            <div className="col-auto pr-3 pb-3">
                                               <FontAwesomeIcon
                                               icon={faDollar}
                                               color={'#00B8B0'}
                                               style={{ width: 45, height: 45 }}
                                               />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <Chart
                        month={month} 
                        customer={customer}
                        transaksi={transaksi}
                        pendapatan={pendapatan}/>  
                        </div>
                    </div>
                    </div>
                </div>
        </div>
        </>
    )
}
export default Dashboard;